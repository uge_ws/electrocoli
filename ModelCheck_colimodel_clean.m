clear all
close all
addpath '/home/eugenio/Scaricati/matlab/cobratoolbox'

initCobraToolbox(false)

changeCobraSolver('ibm_cplex','all')

%loading model

colimodel = readCbModel('colimodel_real.mat')

colimodel.c = zeros(1, length(colimodel.rxns))

findRxnIDs(colimodel, 'celltransport_e_1')
colimodel.rxns(2900)
colimodel.c(2900) = 1
colimodel.c = colimodel.c'
table(colimodel.rxns, colimodel.c)



printRxnFormula(colimodel, 'celltransport_e' )

printRxnFormula(colimodel, 'celltransport_e_1' )


length(colimodel.rxns(findExcRxns(colimodel)));

EX_Sink_reactions = colimodel.rxns(findExcRxns(colimodel));
EX_Sink_reactions(1:357);
colimodel_all_closed = changeRxnBounds(colimodel, EX_Sink_reactions(1:357), 0, 'l');
optimizeCbModel(colimodel_all_closed)
%carica colimodel_real salvato
colimodel_real = colimodel_all_closed;

colimodel_real = changeRxnBounds(colimodel_real, EX_Sink_reactions(1:357), 0, 'l');
%CO2 reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_co2_e',-8.52, 'l');
% Gong, F., Liu, G., Zhai, X., Zhou, J., Cai, Z., Li, Y., 2015. Quantitative 
%analysis of an engineered CO2-fixing Escherichia coli reveals great potential 
%of heterotrophic CO2 fixation. Biotechnol. Biofuels 8, 86. 
%doi:10.1186/s13068-015-0268-1
% 22.5 mg CO2 g DCW(-1) h(-1) transformed in umol°min^-1 gDCW^-1 = 8.52



%ammonium reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_nh4_e', -1000, 'l');
%water reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_h2o_e', -1000, 'l');
%cloride reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_cl_e', -1000, 'l');
%calcium reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_ca2_e', -1000, 'l');
%cobalt reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_cobalt2_e', -1000, 'l');
%copper reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_cu2_e', -1000, 'l');
%iron reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_fe2_e', -1000, 'l');
%iron reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_fe3_e', -1000, 'l');
%proton reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_h_e', -1000, 'l');
%carbonate reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_hco3_e', -0, 'l');
%potassium reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_k_e', -1000, 'l');
%magnesium reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_mg2_e', -1000, 'l');
%manganese reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_mobd_e', -1000, 'l');
%manganese reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_mn2_e', -1000, 'l');
%sodium reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_na1_e', -1000, 'l');
%nickel reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_ni2_e', -1000, 'l');
%zinc reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_zn2_e', -1000, 'l');
%phosphate reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_pi_e', -1000, 'l');
%sulphate reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_so4_e', -1000, 'l');
%sulphate reaction
colimodel_real = changeRxnBounds(colimodel_real, 'EX_pydx_e', -1000, 'l');
% Simulating citrate sinthase to reversible reaction from D. Acetivorans
colimodel_real = changeRxnBounds(colimodel_real, 'CSp', -1000,'l');
colimodel_real = changeRxnBounds(colimodel_real, 'CSp', 0,'u');

colimodel_real = addReaction(colimodel_real,...
'NADPHQR3rev' , ...
'reactionFormula' , 'q8h2[C_c] + 2 nadp[C_c]  -> 2 h[C_c] + q8[C_c] + 2 nadph[C_c]', ...
'reversible' , false , ...
'lowerBound' , 0, ...
'upperBound' , 1000);

colimodel_real = addReaction(colimodel_real, 'CymA_electrn_injection', 'reactionFormula', '4 electrn[C_p] + 2 h[C_p] + q8[C_c] 	->	q8h2[C_c]','reversible' ,false,'lowerBound' , 0, 'upperBound',1000);

colimodel_real = addReaction(colimodel_real, 'NADH6_R', 'reactionFormula', 'q8h2[C_c] + 2 nad[C_c] 	->	2 h[C_c] + q8[C_c] + 2 nadh[C_c]', 'reversible' ,false, 'lowerBound' ,0 , 'upperBound',1000);
colimodel_real = addReaction(colimodel_real, 'Extracellular_electron_uptake', 'reactionFormula', 'electrn[C_e] 	->	electrn[C_p]','reversible' ,false,'lowerBound' , 0, 'upperBound',1000);
colimodel_real = addReaction(colimodel_real, 'Extracellular_electron_uptake_1', 'reactionFormula', '	electrn[C_e] 	->	','reversible' , false, 'lowerBound' ,-1000, 'upperBound',0);
colimodel_real = changeRxnBounds(colimodel_real, 'Growth', 1000,'u');
colimodel_real = changeRxnBounds(colimodel_real, 'CymA_electrn_injection', -1000, 'l');
colimodel_real = changeRxnBounds(colimodel_real, 'cellsyn', 1000, 'u');
colimodel_real = changeObjective(colimodel_real,{'celltransport_e', 'Growth'}, [1,1]);

writeCbModel(colimodel_real, 'colimodel_real.mat');

sol_CO2 = optimizeCbModel(colimodel_real);
disp(sol_CO2.x(2899)), disp(colimodel_real.rxns(2899));
%cellsyn = 0.71 umol cellobiose°min^-1 gDCW^-1 or 1.42 umol glucose°min^-1 gDCW^-1

disp('Optimization solution for maximal production of cellulose without any growth')
disp(newline)
disp(colimodel_real.rxns(2899)), disp(sol_CO2.x(2899));
disp(colimodel_real.rxns(1608)),disp(sol_CO2.x(1608));
disp(newline)
% printFluxVector(colimodel_real,sol_CO2.x,true,false, 0,[] ,[] ,false)
% printRxnFormula(colimodel_real, 'cellsyn');



colimodel_real = changeRxnBounds(colimodel_real, 'cellsyn', 0.70, 'u');
colimodel_real = changeRxnBounds(colimodel_real, 'Growth', 0.001, 'u');
colimodel_real = changeObjective(colimodel_real,{'celltransport_e', 'Growth'}, [1.0, 1.0]);


writeCbModel(colimodel_real, 'colimodel_real.mat');

sol_CO2 = optimizeCbModel(colimodel_real);

disp('Optimization solution for maximal production of cellulose with minimal growth')
disp(newline)
disp(colimodel_real.rxns(2899)), disp(sol_CO2.x(2899));
disp(colimodel_real.rxns(1608)),disp(sol_CO2.x(1608));